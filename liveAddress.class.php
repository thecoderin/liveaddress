<?php
/**
 * @category AddressCheck
 * @package US ZipCode Verification
 * @uses SmartyStreets API http://smartystreets.com/
 * @author Anish Karim <thecoderin@gmail.com>
 * @version 0.1.1
 */

class liveAddress {
  /**
   * Function Construct
   */
  function __construct() {
    $this->authId = NULL;
    $this->authToken = NULL;
  }
  
  /**
   * Function suggestZip
   * @param String $city City Name
   * @param String $state US State Code.
   * @return JSON zipCodes.
   */
  public function suggestZip($city='New York', $state = 'NY', $zip = NULL) {
    if ($this->authId && $this->authToken) {
      $URL = "https://api.smartystreets.com/zipcode?auth-id=" . $this->authId. "&auth-token=" . $this->authToken. "&city=" .urlencode($city) . "&state=" . urlencode($state);
      if (!empty($zip)) {
        $URL .= "&zipcode=" . $zip;
      }
      $output = file_get_contents($URL);
      $array = json_decode($output);
      if (!isset($array[0]->status)) {
        $zipCodes = $array[0]->zipcodes;
        $resultArray = array();
        $count  = 0;
        foreach ($zipCodes as $areaObj) {
          $resultArray[$count] = array(
            'zipcode' => $areaObj->zipcode,
            'lat' => $areaObj->latitude,
            'long' => $areaObj->longitude
          );
          $count++;
        }
        return json_encode($resultArray);
      } else {
        return json_encode(array(
          'error' => 1,
          'desc' => $array[0]->reason
        ));
      }
    }
  }
}